########################################################################
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
########################################################################

import os.path


raw_html_404 = """<!DOCTYPE html>
<html>
<body>
<h1>404: No Such File!</h1>
</body>
</html>
"""

def get_content_type(filepath):
    ext = os.path.splitext(filepath)[1][1:]
    return get_content_type.map.get(ext, 'text/plain')

get_content_type.map = {
    'html': 'text/html; charset=utf-8',
    'js': 'text/javascript',
    'css': 'text/css'
}


def handle(environ, start_response, file_path):
    content_type = get_content_type(file_path)
    if os.path.isfile(file_path):
        response_body = open(file_path).read().encode('utf-8')
    else:
        response_body = raw_html_404.encode('utf-8')
        content_type = 'text/html; charset=utf-8'

    status = '200 OK'
    headers = [('Content-type', content_type),
               ('Content-Length', str(len(response_body)))]
    start_response(status, headers)
    return [response_body]
