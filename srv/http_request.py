########################################################################
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
########################################################################

import json
import ixion


def get_column_names(start, length):
    stop = start + length
    return ixion.column_label(start, stop)


def handle(environ, start_response):
    request_body_size = int(environ['CONTENT_LENGTH'])
    request_body = environ['wsgi.input'].read(request_body_size)
    request = json.loads(request_body.decode('utf-8'))
    func = globals()[request['function']]
    result = {'answer': func(*request['args'], **request['kwargs'])}
    response_body = json.dumps(result).encode('utf-8')
    status = '200 OK'
    headers = [('Content-type', 'text/plain; charset=utf-8')]
    start_response(status, headers)
    return [response_body]



