########################################################################
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
########################################################################

invalid_command_html = """
<!DOCTYPE html>
<html>
<body>
Invalid debug command: {cmd}
</body>
</html>
"""

def handle(environ, start_response, cmd):

    if cmd == 'dump':
        response_body = ''
        for k, v in environ.items():
            response_body += "{}: {}\n".format(k, v)
        response_body = response_body.encode('utf-8')
        status = '200 OK'
        headers = [('Content-type', 'text/plain; charset=utf-8'),
                   ('Content-Length', str(len(response_body)))]
        start_response(status, headers)
        return [response_body]

    response_body = invalid_command_html.format(cmd=cmd).encode('utf-8')
    status = '200 OK'
    headers = [('Content-type', 'text/html; charset=utf-8'),
               ('Content-Length', str(len(response_body)))]
    start_response(status, headers)
    return [response_body]




