#!/usr/bin/env python3
########################################################################
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
########################################################################

import os.path
import srv.debug
import srv.http_request
import srv.file_request
from wsgiref.simple_server import make_server


PORT = 8080


def eris_app(environ, start_response):
    if environ['REQUEST_METHOD'] == 'POST':
        return srv.http_request.handle(environ, start_response)

    server_path = environ['PATH_INFO']
    if server_path.startswith('/debug/'):
        return srv.debug.handle(environ, start_response, server_path[7:])

    if server_path == '/':
        server_path += 'html/index.html'

    file_path = os.path.dirname(os.path.abspath(__file__))
    file_path += server_path
    print(file_path)
    return srv.file_request.handle(environ, start_response, file_path)


def start_server():
    httpd = make_server("", PORT, eris_app)
    httpd.serve_forever()


if __name__ == "__main__":
    start_server()

