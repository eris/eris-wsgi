/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

function drawReset(cv) {
    var cxt = cv.getContext("2d");
    cxt.fillStyle = "green";
    cxt.fillRect(10, 10, 100, 100);
}

function drawMouseClickAt(cv, x, y) {
    var radius = 10;
    var cxt = cv.getContext("2d");
    cxt.beginPath();
    cxt.arc(x, y, radius, 0, 2 * Math.PI, false);
    cxt.fillStyle = 'red';
    cxt.fill();
    cxt.lineWidth = 2;
    cxt.strokeStyle = '#003300';
    cxt.stroke();
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
