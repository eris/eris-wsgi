/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

var colSize = 10;
var rowSize = 20;

var currentCell = null;
var inputMode = false;
var oldCellValue = "";

function sendRequest(data, callback) {
    var req = new XMLHttpRequest();
    req.open("POST", '', true);
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            var res = JSON.parse(req.responseText);
            callback(res);
        }
    }
    req.send(JSON.stringify(data));
}

function fillColumnHeader() {
    var obj = {
        'function': 'get_column_names',
        'args': [0, colSize], 'kwargs': {}
    };

    sendRequest(obj, function (res) {
        var answer = res['answer'];
        var cols = document.querySelectorAll("#id_sheet_main tr:first-child td.column-header");
        for (var i = 0; i < cols.length; ++i) {
            if (answer[i]) {
                cols[i].innerHTML = answer[i];
            }
        }
    });
}

function onPageLoad() {

    var cv = document.getElementById("canvas_main");
    drawReset(cv);
    cv.onclick = canvasOnClick;

    var table = document.querySelector("#id_sheet_main");
    if (!table) {
        return;
    }

    var colgroup = table.querySelector("colgroup");
    if (colgroup) {
        // Insert column size + 1 for the row header.
        for (var i = 0; i < (colSize+1); ++i) {
            var col = document.createElement("col");
            colgroup.appendChild(col);
        }
    }

    var tbody = table.querySelector("tbody");
    if (!tbody) {
        return;
    }

    for (var i = 0; i < (rowSize+1); ++i) {
        var row = document.createElement("tr");
        tbody.appendChild(row);
        for (var j = 0; j < (colSize+1); ++j) {
            var cell = document.createElement("td");
            cell.dataset.row = i-1;
            cell.dataset.column = j-1;
            if (i > 0 && j > 0) {
                // editable cell
                cell.contentEditable = false;
//              cell.onkeydown = onCellKeyDown;
//              cell.onkeypress = onCellKeyPress;
                cell.onclick = onCellClick;
                cell.classList.add("cell");
            } else if (i === 0 && j === 0) {
                // Top left corner cell.
            } else if (i === 0) {
                cell.classList.add("column-header");
            } else if (j === 0) {
                cell.classList.add("row-header");
                cell.innerHTML = i;
            }
            row.appendChild(cell);
        }
    }

    fillColumnHeader();

    // Set initial focus on cell A1.
    var query = '#id_sheet_main td[data-row="0"][data-column="0"]';
    var cell = document.querySelector(query);
    cell.classList.add("focused");
    currentCell = cell;
}

function canvasOnClick(e) {
    var cv = e.target;
    var rect = cv.getBoundingClientRect();
    var x = e.clientX - rect.left;
    var y = e.clientY - rect.top;
    drawMouseClickAt(cv, x, y);
}

function moveCursorUp() {
    if (!currentCell) {
        return;
    }

    endInputMode(currentCell);

    var row = parseInt(currentCell.dataset.row);
    var col = parseInt(currentCell.dataset.column);

    if (row === 0) {
        // top row
        return;
    }

    --row;
    setFocusOnCell(row, col);
    inputMode = false;
}

function moveCursorDown() {
    if (!currentCell) {
        return;
    }

    endInputMode(currentCell);

    var row = parseInt(currentCell.dataset.row);
    var col = parseInt(currentCell.dataset.column);

    if (row === rowSize-1) {
        // bottom row
        return;
    }

    ++row;
    setFocusOnCell(row, col);
    inputMode = false;
}

function moveCursorLeft() {
    if (!currentCell) {
        return;
    }

    endInputMode(currentCell);

    var row = parseInt(currentCell.dataset.row);
    var col = parseInt(currentCell.dataset.column);

    if (col === 0) {
        // left-most column.
        return;
    }

    --col;
    setFocusOnCell(row, col);
    inputMode = false;
}

function moveCursorRight() {
    if (!currentCell) {
        return;
    }

    endInputMode(currentCell);

    var row = parseInt(currentCell.dataset.row);
    var col = parseInt(currentCell.dataset.column);

    if (col === colSize-1) {
        // right-most column.
        return;
    }

    ++col;
    setFocusOnCell(row, col);
    inputMode = false;
}

function setFocusOnCell(row, col) {
    var query = '#id_sheet_main td[data-row="' + row + '"][data-column="' + col + '"]';
    var cell = document.querySelector(query);
    currentCell.classList.remove("focused");
    cell.classList.add("focused");
    currentCell = cell;
}

function onCellKeyDown(e) {
    return;
    console.log(e);
    switch (e.key) {
    case "ArrowDown":
        // Move focus on the next cell down.
        moveCursorDown();
        break;
    case "ArrowUp":
        // Move focus to the next cell up.
        moveCursorUp();
        break;
    }
}

function onCellKeyPress(e) {
    console.log(e);
    switch (e.key) {
    case "Enter":
    case "ArrowDown":
    case "ArrowUp":
        return false;
    }

    return true;
}

function onCellClick(evt) {
    if (currentCell) {
        currentCell.classList.remove("focused");
    }

    var cell = evt.target;
    cell.classList.add("focused");
    currentCell = cell;
}

function onKeyDown(evt) {
}

function onKeyUp(evt) {
}

function startInputMode(cell, charCode) {

    oldCellValue = cell.textContent;

    cell.contentEditable = true;
    cell.focus();
    cell.textContent = String.fromCharCode(charCode);

    var range = document.createRange();
    range.setStart(cell.childNodes[0], 1);
    range.collapse(true);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);

    inputMode = true;
}

function endInputMode(cell) {
    cell.blur();
    cell.contentEditable = false;
    inputMode = false;
}

function handleEscape() {
    if (!inputMode) {
        return;
    }

    currentCell.textContent = oldCellValue;
    endInputMode(currentCell);
}

function handleEnter() {
    if (inputMode) {
        endInputMode(currentCell);
    }
    moveCursorDown();
}

function handleTab(evt) {
    evt.preventDefault();
    if (inputMode) {
        endInputMode(currentCell);
    }
    var isShift = !!evt.shiftKey;
    if (isShift) {
        moveCursorLeft();
    } else {
        moveCursorRight();
    }
}

function onKeyPress(evt) {
    console.log(evt);
    switch (evt.keyCode) {
    case 38:
        // arrow up
        moveCursorUp();
        return;
    case 40:
        // arrow down
        moveCursorDown();
        return;
    case 39:
        // arrow right
        moveCursorRight();
        return;
    case 37:
        // arrow left
        moveCursorLeft();
        return;
    case 27:
        // ESC
        handleEscape();
        return;
    case 13:
        // Enter
        handleEnter();
        return;
    case 9:
        // Tab
        handleTab(evt);
        return;
    }

    if (evt.charCode < 32 || 126 < evt.charCode) {
        // Outside the ASCII range.
        return;
    }

    if (!inputMode) {
        startInputMode(currentCell, evt.charCode);
    }
}

window.addEventListener("load", onPageLoad);
window.addEventListener("keydown", onKeyDown, true);
window.addEventListener("keyup", onKeyUp, true);
window.addEventListener("keypress", onKeyPress, true);

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
